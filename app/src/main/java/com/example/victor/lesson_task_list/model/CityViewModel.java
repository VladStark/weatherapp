package com.example.victor.lesson_task_list.model;

public class CityViewModel {
    private String title;
    private WeatherState state;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public WeatherState getState() {
        return state;
    }

    public void setState(WeatherState state) {
        this.state = state;
    }

    public float getTemperature() {
        return state.getTemperature();
    }

    public int getWindSpeed() {
        return state.getWindSpeed();
    }

    public String getWeatherState() {return state.getForecast();}
}
