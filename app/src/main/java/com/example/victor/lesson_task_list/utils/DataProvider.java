package com.example.victor.lesson_task_list.utils;

import com.example.victor.lesson_task_list.model.WeatherState;

import java.util.HashMap;

public class DataProvider {

    public static final String STATE_RAINY = "storm";
    public static final String STATE_SNOW = "snowing";
    public static final String STATE_CLEAR = "sun";
    public static final String STATE_CLOWD = "clouded";


    private static final String[] CITIES = {"Волгоград", "Саратов", "Астрахань", "Владивосток", "Москва",
                    "Санкт-Петербург", "Новосибирск", "Екатеренбург", "Воронеж"};

    public static HashMap<String, WeatherState> getData() {
        HashMap<String, WeatherState> data = new HashMap<>();

        for (String city : CITIES) {
            WeatherState weatherState = new WeatherState();
            weatherState.setTemperature((int) (Math.random() * 25 - 15));
            weatherState.setWindSpeed((int) (Math.random() * 15 + 3));
            weatherState.setForecast(getRandomState(weatherState.getTemperature()));

            data.put(city, weatherState);
        }

        return data;
    }

    private static String getRandomState(int t) {
        int random = (int) (Math.random() * 4);
        switch (random) {
            case 0 : if (t <= 0)
                return STATE_SNOW;
            case 1 : return STATE_CLOWD;
            case 2 : return STATE_RAINY;
            default: return STATE_CLEAR;
        }

    }

}
