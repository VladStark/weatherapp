package com.example.victor.lesson_task_list.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.victor.lesson_task_list.R;
import com.example.victor.lesson_task_list.model.CityViewModel;

import java.util.ArrayList;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private ArrayList<CityViewModel> mDataList = new ArrayList<>();
    private Context mContext;

    public CityAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_item, null);
        CityViewHolder holder = new CityViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        CityViewModel model = mDataList.get(position);
        holder.mCityTitle.setText(" " + model.getTitle());
        holder.mTemperature.setText(String.valueOf(model.getTemperature() + " C*"));
        holder.mWindSpeed.setText(String.valueOf("V ветра = "+model.getWindSpeed()));
        int drawableResourceId = mContext.getResources().getIdentifier(model.getWeatherState(), "drawable", mContext.getPackageName());
        holder.mStateImage.setImageResource(drawableResourceId);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void setData(ArrayList<CityViewModel> data) {
        mDataList = data;
    }

    class CityViewHolder extends RecyclerView.ViewHolder {

        private ImageView mStateImage;
        private TextView mCityTitle, mTemperature, mWindSpeed;

        public CityViewHolder(View rootView) {
            super(rootView);
            mCityTitle = (TextView) rootView.findViewById(R.id.city_title);
            mTemperature = (TextView) rootView.findViewById(R.id.temperature_hint);
            mWindSpeed = (TextView) rootView.findViewById(R.id.wind_speed);
            mStateImage = (ImageView) rootView.findViewById(R.id.temperature);
        }
    }
}
